package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.ArrayList;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {
        Park park;
        try {
            Constructor<Park> constructor = Park.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            park = constructor.newInstance();
            Field[] fields = park.getClass().getDeclaredFields();
            constructor.setAccessible(false);
            FileReader fileReader = new FileReader(new File(parkDatafilePath));
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] data = line.split(": ");
                if (!data[0].equals("***")) {
                    String fieldName = data[0].substring(1, data[0].length() - 1);
                    String fieldValue = data[1].substring(1, data[1].length() - 1);
                    for (Field field : fields) {
                        field.setAccessible(true);
                        if (field.isAnnotationPresent(FieldName.class)) {
                            if (field.getAnnotation(FieldName.class).value().equals(fieldName)) {
                                field.set(park, fieldValue);
                            }
                        }
                        if (field.isAnnotationPresent(MaxLength.class)) {
                            if (field.getName().equals(fieldName) && field.getAnnotation(MaxLength.class).value() >= fieldName.length()) {
                                field.set(park, fieldValue);
                            }
                        }
                        if (field.isAnnotationPresent(NotBlank.class)) {
                            if (field.getName().equals(fieldName) && !fieldName.equals("null")) {
                                String[] ld = fieldValue.split("-");
                                Constructor<LocalDate> c = LocalDate.class.getDeclaredConstructor(int.class, int.class, int.class);
                                c.setAccessible(true);
                                LocalDate localDate = c.newInstance(Integer.parseInt(ld[0]), Integer.parseInt(ld[1]), Integer.parseInt(ld[2]));
                                c.setAccessible(false);
                                field.set(park, localDate);
                            }
                        }
                        field.setAccessible(false);
                    }
                    for (Field field : fields) {
                        boolean flag = false;
                        ParkParsingException ex = new ParkParsingException("Ошибки парсинга", new ArrayList<>());
                        if (field.getName().equals("legalName") && field.get("legalNme") == null) {
                            ex.getValidationErrors().add(new ParkParsingException.ParkValidationError(field.getName(),"Не найдено значение для legal name"));
                            flag = true;
                        }
                        if (field.getName().equals("ownerOrganizationInn") && field.get("ownerOrganizationInn") == null) {
                            ex.getValidationErrors().add(new ParkParsingException.ParkValidationError(field.getName(),"Превышена длина"));
                            flag = true;
                        }
                        if (field.getName().equals("foundationYear") && field.get("foundationYear") == null) {
                            ex.getValidationErrors().add(new ParkParsingException.ParkValidationError(field.getName(),"Поле равно null"));
                            flag = true;
                        }
                        if (flag) {
                            throw ex;
                        }

                    }
                }
            }
            bufferedReader.close();
            fileReader.close();
            return park;
        } catch (IOException | IllegalAccessException | InstantiationException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

}
